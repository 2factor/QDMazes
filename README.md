## Deceptive Mazes Testbed for Quality Diversity

This testbed contains 60 procedurally generated mazes.

Mazes are generated via a recursive division algorithm, which subdivides the maze (by adding walls at the border) recursively until no more walls can be added. 
The algorithm stops after a specific number of subdivisions,
or when adding a new wall would make the path non-traversable. 
The start position is always set on the bottom-left corner and the goal position on the top-right corner.
Width of gaps and the minimum width of corridors is defined in a way that allows the robot controller to comfortably pass through.
All mazes tested have between 5 and 12 subdivisions (chosen randomly).

## Selection criteria

This set is an attempt to find appropriate deceptive mazes which satisfy two criteria related to the algorithms tested rather than inherent structural patterns.
These criteria are:
- Objective search must not find any solutions in 50 evolutionary runs.
- Novelty search with local competition (NS-LC) [1] must find a solution in at least one of 50 evolutionary runs.

The first criterion establishes that each maze is deceptive, as attempting to optimize proximity to the goal does not result in any solutions. 
To ensure that the maze is not too difficult (or practically impossible), the QD algorithm  NS-LC is used as a second criterion:
if NS-LC cannot find a solution even after numerous retries, then the maze is characterized as too difficult and is ignored.

The "Mazes" folder contains the complete set of 60 mazes, while the "Sensitivity" folder contains the 10 easiest mazes (taken from the complete set) based on the number of successes obtained by NS-LC.

## Visualization
	
The repository contains a parser (`ShowMaze.py`) to save the mazes as .png file.

### Python Requirements 

* Python 3.6
* Matplotlib >= 2.02

## Additional Notes

The selected maze descriptor is based on the maze encoding used in the original novelty search source code provided by Joel Lehman and Kenneth O. Stanley [2].

## References

[1] Lehman, Joel, and Kenneth O. Stanley. "Evolving a diversity of virtual creatures through novelty search and local competition." Proceedings of the 13th annual conference on Genetic and evolutionary computation. ACM, 2011.

[2] Lehman, Joel, and Kenneth O. Stanley. "Abandoning objectives: Evolution through the search for novelty alone." Evolutionary computation 19.2 (2011): 189-223.

----
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/80x15.png" /></a><br />
<span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">Deceptive Mazes Testbed for Quality Diversity</span>
is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
